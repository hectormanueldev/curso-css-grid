# Curso de CSS Grid

## Temario

1. :heavy_plus_sign: Grid explicita
1. :heavy_plus_sign: Grid line names
1. :heavy_plus_sign: Grid areas
1. :heavy_plus_sign: Grid implícita
1. :heavy_plus_sign: Grid flow
1. :heavy_plus_sign: Grid flow dense
1. :heavy_plus_sign: Grid layer
1. :heavy_plus_sign: Grid order
1. :heavy_plus_sign: Grid align
1. :heavy_plus_sign: Align tracks
1. :heavy_plus_sign: Grid min max
1. :heavy_plus_sign: Grid repeat
1. :heavy_plus_sign: Grid dynamics
1. :heavy_plus_sign: Grid responsive
1. :heavy_plus_sign: Grid nested
1. :heavy_plus_sign: Subgrids
1. :heavy_plus_sign: Responsive con grid lines
1. :heavy_plus_sign: Responsive con grid areas
